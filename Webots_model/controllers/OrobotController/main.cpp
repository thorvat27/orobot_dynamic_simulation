#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <vector>
#include <sstream>
#include <string.h>
#include <stdlib.h>
#include "controller.hpp"
#include "robotSim.hpp"
#include "joystick.h"
#include "OptiCtrl.hpp"



#define TIME_STEP   10    //[ms]
#define MAX_NUM_MOTORS   40

// load global config
int readGlobalConfig();


using namespace std;
using namespace Eigen;
using namespace webots;

// GLOBAL CONFIG
int IS_SIMULATION;
int IS_OPTIMIZATION;
int USE_JOYSTICK;
int SPINE_COMPENSATION_WITH_FEEDBACK;
int USE_IMU;
int USED_MOTORS[50];
int AUTO_RUN, AUTO_RUN_STATE;
int LOG_DATA, RECORD_VIDEOS;
int ANIMAL_DATASET;
int JOYSTICK_RECORDING_REPLAYING;
int USE_FEET;
int SKIP_WRIST_SERVOS;
int NUM_MOTORS;
int SPINE_SIZE;

//###################### MAIN PROGRAM  - connects Controller and RobotSim classes ####################################################

int main(int argc, char **argv)
{
    if(readGlobalConfig()==0){
        return 0;
    }
    USE_FEET=USE_IMU;
    SKIP_WRIST_SERVOS=!USE_FEET;

    cout<<"MAIN STARTS"<<endl;

    double Td = TIME_STEP/1000.;
    double angles_reference[MAX_NUM_MOTORS];


    //========================= Initialize the robot and controller =========================

    RobotSim robotSim(TIME_STEP);
    cout<<"ROBOT CREATED"<<endl;

    cout << "creating controller" << endl;    
    Controller controller(Td);
    
    cout<<"CONTROLLER CREATED"<<endl;

    //============================ CLUSTER for the grid search =====================================
    OptiCtrl optictrl;
    if(IS_OPTIMIZATION){
        cout<<"OPTICTRL CREATED"<<endl;
        optictrl.optimizationInit(&controller, &robotSim);
    }
    //==========================================================================
    controller.initOrobot();

    double t=0;
    double dt;
    int numsteps=0;

    VectorXd torques(NUM_MOTORS), position(NUM_MOTORS);
    double angles_feedback[MAX_NUM_MOTORS], torques_feedback[MAX_NUM_MOTORS];
    double fgird_rotMat[9], gpsData[3], GRF[12];
    double FL_feet_gpos[3], FR_feet_gpos[3], HL_feet_gpos[3], HR_feet_gpos[3];

    //================================== FREQ DEPENDANT FEET PARAMETERS ================================
    robotSim.set2segFeetParam(2.5, 2.5*0.25); // 0.5hz
    //robotSim.set2segFeetParam(3.5, 3.5*0.25); // 0.75hz 


    
cout << "STARTING THE MAIN LOOP" << endl;
//=============================  LOOP  =============================================
    while(robotSim.step(TIME_STEP) != -1) {

        dt=Td;
        t=t+dt;
        controller.setTimeStep(dt);


        //read from robot
        robotSim.getPositionFeedback(angles_feedback);        //joint positions
        robotSim.getTorqueFeedback(torques_feedback);       // joint torques
        robotSim.ReadIMUAttitude(fgird_rotMat); //front girdle rotation matrix
        robotSim.GetGPS(gpsData);          // front girdle position
        robotSim.GetFeetGPS(FL_feet_gpos, FR_feet_gpos, HL_feet_gpos, HR_feet_gpos);
        robotSim.getGRF(GRF); 

        // update the controller
        controller.updateRobotState(angles_feedback, torques_feedback);
        controller.getAttitude(fgird_rotMat);
        controller.getGPS(gpsData, FL_feet_gpos, FR_feet_gpos, HL_feet_gpos, HR_feet_gpos);
        controller.GRFfeedback(GRF);

        if(!controller.runStep()){
            break;
        }

        controller.getAngles(angles_reference);

        robotSim.setAngles(angles_reference, USED_MOTORS);




        //=================================== GRID SEARCH / Optimization calls =========================

        if(IS_OPTIMIZATION){
            if(optictrl.OptiStep(dt, &controller, &robotSim) == -1){
                break;
            }
        }
        //==============================================================================


    }


  return 0;
}



// load global config
int readGlobalConfig()
{
    stringstream stringstream_file;
    ifstream global_config_file("config/GLOBAL.config");
    if(global_config_file.is_open()){
        readFileWithLineSkipping(global_config_file, stringstream_file);
        stringstream_file >> IS_SIMULATION;
        stringstream_file >> IS_OPTIMIZATION;
        stringstream_file >> USE_JOYSTICK;
        stringstream_file >> SPINE_COMPENSATION_WITH_FEEDBACK;
        stringstream_file >> USE_IMU;
        stringstream_file >> AUTO_RUN;
        stringstream_file >> AUTO_RUN_STATE;
        stringstream_file >> LOG_DATA;
        stringstream_file >> RECORD_VIDEOS;
        stringstream_file >> NUM_MOTORS;
        stringstream_file >> SPINE_SIZE;
                
        // IDs
        cout << "USED_IDS: \n";
        for(int i=0; i<NUM_MOTORS; i++){
            stringstream_file >> USED_MOTORS[i];

        }
        stringstream_file >> ANIMAL_DATASET;
        return 1;
    }
        
    else{
        return 0;
    }
}




















