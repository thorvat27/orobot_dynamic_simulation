clc
% clf
close

filepath = './data/'
d05 = importdata([filepath 'dataMatFINAL_4met_05Hz.csv']);
d075 =importdata([filepath 'dataMatFINAL_4met_075Hz.csv']);
dkin =importdata([filepath 'dataMat_kin.csv']);


% CHOSE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
dynHeaders = d05.colheaders;
dynData = d05.data;


% kinParF = dkin.data(:, [1 3 5]); %abduction
% kinParH = dkin.data(:, [2 4 6]);
kinParF = dkin.data(:, [4 6 10]); %body height
kinParH = dkin.data(:, [5 7 10]);
dynDataF = dynData(:, [1 3 10]);
dynDataH = dynData(:, [2 4 10]);

kinData = dkin.data;


kinParFOrig = kinParF;
kinParHOrig = kinParH;
% normalize axis
allRange = [kinParF; kinParH; dynDataF; dynDataH];
for k=1:3
    kinParF(:,k) = (kinParF(:,k)-min(allRange(:,k))) / max(allRange(:,k));
    kinParH(:,k) = (kinParH(:,k)-min(allRange(:,k))) / max(allRange(:,k));
    dynDataF(:,k) = (dynDataF(:,k)-min(allRange(:,k))) / max(allRange(:,k));
    dynDataH(:,k) = (dynDataH(:,k)-min(allRange(:,k))) / max(allRange(:,k));
end

for k=1:size(dynData,1)
    
% for k=222
%     pF = repmat(dynData(k,[1 3 5]), 100, 1);
%     pH = repmat(dynData(k,[2 4 6]), 100, 1);

    pF = repmat(dynDataF(k,:), 100, 1);
    pH = repmat(dynDataH(k,:), 100, 1);
    
    dist = sqrt(sum((pF-kinParF).^2,2));
	indx = find(dist==min(dist));  indxF=indx;
    scoreF(k) = dkin.data(indx(1), 11);
        
    dist = sqrt(sum((pH-kinParH).^2,2));
	indx = find(dist==min(dist));  indxH=indx;
    scoreH(k) = dkin.data(indx, 12);
    
    scoreC(k) = dkin.data(indx, 11) + dkin.data(indx, 12);
    
end


for k=1:size(kinData,1)


    pH = repmat(kinParH(k,:), 100, 1);
    
       
    dist = sqrt(sum((pH-kinParF).^2,2));
	indx = find(dist==min(dist));  indxH=indx;
    scoreKinH(k) = dkin.data(indx, 11) + dkin.data(k, 12) ;
    

    
end

scatter3(kinParH(:,1), kinParH(:,2), kinParH(:,3), kinParH(:,3)*0+400, kinParH(:,3)*0+100); 
% set(gca, 'XDir', 'reverse');
% set(gca, 'YDir', 'reverse');
% figure
hold on
scatter3(dynDataH(:,1), dynDataH(:,2), dynDataH(:,3), scoreH*100, scoreH*100, 'filled');  
set(gca, 'XDir', 'reverse');
set(gca, 'YDir', 'reverse');
%%
outFile='./data/dataMatFINAL_4met_joined_05Hz.csv';
newHeaders = [dynHeaders, {'scoreF', 'scoreH', 'scoreBoth'}];
newLogMat = [dynData scoreF' scoreH' scoreF'+scoreH']
% newLogMat = [dynData scoreF' scoreH' scoreC']
% dlmwrite('scores.csv', [scoreF' scoreH' scoreF'+scoreH']);

%write header to file
textHeader = strjoin(newHeaders, ',');
fid = fopen(outFile,'w'); 
fprintf(fid,'%s\n',textHeader); fclose(fid);

%write data to end of file
dlmwrite(outFile,newLogMat ,'-append');

