clc
close
clear



% LAR | spine | height    | deviations
modern = [ ...
    26.99, 27.29, 0.393, 			7.23, 	4.90, 	0.011; ...  %caiman
    62.26, 30.91, 0.458, 			10.11, 	10.29, 	0.017; ...  %iguana
    85.45, 38.24, 0.211, 			9.83, 	9.71, 	0.008; ...  %salamander
    79.04, 20.72, 0.202, 			13.76, 	11.12, 	0.007; ...  %skink
   
    58.59, 43.97, 0.501, 			11.32,	6.16, 	0.021; ...		
    45.49, 13.04, 0.254, 			18.56,	9.24, 	0.012; ...
    67.86, 41.23, 0.292, 			13.60,	7.05, 	0.019; ...
    45.82, 19.71, 0.154, 			10.18,	1.95, 	0.008; ...
]

% correct scaling
modern(:,2)=modern(:,2)*0.5;
modern(:,5)=modern(:,5)*0.5;
    

logMAT = [];
devScl=1;
for k=1:8
    [x, y, z] = ellipsoid(modern(k,1) , modern(k,2), modern(k,3),           devScl*modern(k,4) , devScl*modern(k,5), devScl*modern(k,6),7);
    x=reshape(x, numel(x),1);
    y=reshape(y, numel(y),1);
    z=reshape(z, numel(z),1);
    
    logMAT = [logMAT, x, y, z];
end

logMAT = [reshape(modern(:, 1:3)', 1, 24); logMAT];
textHeader = ['caimanFX,','caimanFY,','caimanFZ,', ...
              'iguanaFX,','iguanaFY,','iguanaFZ,', ...
              'salamanderFX,','salamanderFY,','salamanderFZ,', ...
              'skinkFX,','skinkFY,','skinkFZ,', ...
              'caimanHX,','caimanHY,','caimanHZ,', ...
              'iguanaHX,','iguanaHY,','iguanaHZ,', ...
              'salamanderHX,','salamanderHY,','salamanderHZ,', ...
              'skinkHX,','skinkHY,','skinkHZ,'];

k=7;
[x, y, z] = ellipsoid(modern(k,1) , modern(k,2), modern(k,3),           devScl*modern(k,4) , devScl*modern(k,5), devScl*modern(k,6),7);
surf(x,y,z)

%% log
% outFile='./data/modernSpecies.csv';
% 
% 
% %write header to file
% fid = fopen(outFile,'w'); 
% fprintf(fid,'%s\n',textHeader); fclose(fid);
% 
% %write data to end of file
% dlmwrite(outFile,logMAT ,'-append');
% 
% 














