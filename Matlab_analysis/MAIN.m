clc
close all
clear all



% filepath='/data/thorvat/orobot/FINAL_075_7744_sparse/';
filepath='/data/thorvat/orobot/FINAL_05_512/';

igScl=1.1;
IG=0.438;
oroMass = 61.0673;  %N


% KK=1:7744
KK=1:1
dataAnalysisAndMetricsCalculation




%% METRICS (all variations)

energyTV=1./torquesVelocity;
energyTVAbs=1./torquesVelocityAbs;
energyTVZeroIg=1./torquesVelocityZeroIg;
energyTS=1./torquesSquared;
maxTorquePerc = 1./trqPercentile; %80,85,90,95%

stanceTorque = 1./ avgTorqueMidStance;

footTrajectoryReachability = 1./trackingErrorStance;
stability = 1./rollPitchNorm;
stabilityRate = 1./rollPitchNormRate;
absolutePrecision = 1./footstepsAdjustmentCost;
smoothness = 1./highFreqComp;   %1,2,3,4,5Hz


crit = {energyTV, energyTVAbs, energyTVZeroIg, ...  %3
        energyTS, footTrajectoryReachability, stability, stabilityRate, ...  %7
        absolutePrecision, ... %8
        maxTorquePerc', smoothness', stanceTorque,  ...
        GRFcorrM_ds(:,2)', GRFabsM_ds(:,2)', GRFsqM_ds(:,2)'};  %12, 13, 14
    
critStr = {'energyTV', 'energyTVAbs', 'energyTVZeroIg', 'energyTS', ...
    'footTrajectoryReachability', 'stability', 'stabilityRate', 'absolutePrecision', ...
    'maxTorquePerc80','maxTorquePerc85','maxTorquePerc90','maxTorquePerc95', ...
    'smoothness1','smoothness2','smoothness3','smoothness4','smoothness5', ...
    'stanceTorque', 'GRFcorrM_ds', 'GRFabsM_ds', 'GRFsqM_ds'};




paramF.ax1=longAxisRotF*180/pi;
paramF.ax2=spineBendingParameters*180/pi;
paramF.ax3=abductionF*180/pi;

paramH.ax1=longAxisRotH*180/pi;
paramH.ax2=spineBendingParameters*180/pi;
paramH.ax3=abductionH*180/pi;

bodyHeightIGD = abs(bodyHeightParameters)/IG;

if girdOrientationPresent{1} ==1
    paramF.ax2=girdAmpF*180/pi;
    paramH.ax2=girdAmpH*180/pi;
end
% asd
%% FEET PARAMETERS
if size(par_grid,1) == 5
    processFeetParamSearch
end

%% TRAJ OFFSET PARAMETERS
if size(par_grid,1) == 4
    processTrajOffParamSearch
end

%% plot
figure
critNum=12;
critSubIndx=1;
clf
plotOptilog(crit{critNum}(2,:), critStr{critNum}, paramF, settings, 5)



%% log for the website

% outFile='/home/thorvat/Dropbox/OrobotProject/JAVASCRIPT/data/dataMatFINAL_4met_075Hz.csv';
% 
% cHeader = {'longAxisRotF' 'longAxisRotH' 'spineBendingParametersF' 'spineBendingParametersH' 'abductionF' 'abductionH'}; 
% cHeader = [cHeader {'bodyHeightParameters', 'spineBendingParameters', 'yawVsRollParameters', 'bodyHeightIGD'}];
% cHeader = [cHeader critStr];
% 
% 
% logMAT = [paramF.ax1', paramH.ax1', paramF.ax2' paramH.ax2', paramF.ax3', paramH.ax3'];
% logMAT = [logMAT, bodyHeightParameters', spineBendingParameters', yawVsRollParameters', bodyHeightIGD'];
%     
% for k=1:numel(crit)
%     size(crit{k}')
%     logMAT = [logMAT crit{k}'];
% end
% 
% 
% 
% %write header to file
% textHeader = strjoin(cHeader, ',');
% fid = fopen(outFile,'w'); 
% fprintf(fid,'%s\n',textHeader); fclose(fid);
% 
% %write data to end of file
% dlmwrite(outFile,logMAT ,'-append');





