# OroBOT dynamic simulation
------

This repository contains dynamic model of OroBOT in Webots and Matlab functions and scripts used to analyse data obtained from the simulation.

### Software requirements
* Ubuntu 14.04 (tested)
* Webots 8.6.2 (recommended)
* Matlab R2016b (recommended)

##### Libraries required by Webots controller
* qpOASES 3.2.0
* liboptimization for evaluation on biorob cluster. It chould be installed even if no cluster will be used to successfully compile the controller: https://biorob.epfl.ch/page-36418-en.html 
* Eigen linear algebra library
* dLib 19.0 C++ library


### Running the Webots model
------
After installing the correct Webots version and needed dependencies, make sure all the paths within controller's Makefile are set correctly (OroBOT_Dynamic_Simulation/Webots_model/controllers/OrobotController/Makefile). The controller should be recompiled before the first run. 

The locomotion parameters can be modified by changing their values inside files `orobotSpecific.config` and `ikinController.config` which are located within config directory. The main sprawling posture **gait parameters** that dictate spine bending (`or_spineCPGscaling`), body height (`or_heightF, or_heightH`) and long axis rotation (`or_ROLLvsYAW`) as well as the walking frequency (`or_walk_freq`) are located inside `orobotSpecific.config`. 




### Data analysis in MATLAB
------
The files presented here contain Matlab functions and scripts used to analyse the log files of OroBOT dynamic simulation recorded in Webots.
